---
layout: default
title: Transformers
nav_order: 4
mathjax: true
---

# Transformers
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

# AC to AC conversion

- [Transformers Summary](https://www.electronics-tutorials.ws/transformer/transformer-basics.html)
- It's comparatively simple to trade voltage and current in the AC world...

![alt_text](media/image26.png "image_tooltip")

# AC to DC conversion

- [Full Wave Rectifier](https://www.electronics-tutorials.ws/diode/diode_6.html)
- On that note, why do we use 60Hz sinusoidal AC power here in the US, when [DC power transmission is less lossy due to the skin effect](https://www.allaboutcircuits.com/textbook/alternating-current/chpt-3/more-on-the-skin-effect/)?
- The answer as we will discuss later in the motors section, has to do with the fact that that power generation (ie. spinning electromagnets) often produces in sinusoidal waveforms, and our power distribution network predates the switching power supply and readily available diodes for rectification.
- AC allows us to inexpensively convert between voltages using transformers (where we prefer long distance transmission to happen at higher voltages). It's also safer for domestic applications. We also switch at relatively low frequencies to consider loss.

![alt_text](media/image27.png "image_tooltip")

---

# supplementary materials
This [New Yorker article](https://www.newyorker.com/magazine/2024/03/04/what-a-major-solar-storm-could-do-to-our-planet) on the catastrophic consequences of a solar storm (2025 is a peak year for solar activity, yay!) illustrates just how crucial transformers are!
> The power grid is built for alternating current, but geomagnetically induced currents are basically direct. The collision of these two currents can lead to the inability to transfer power efficiently, large temperature spikes inside transformers (which emit unholy groans and bangs under the strain), relays and other equipment tripping off-line, and, on a very bad day, voltage collapse...
> “Transformers are not just something you can go to Home Depot and buy,” Baker points out; each one is idiosyncratic, a half-million-pound object designed specifically for one of the fifteen hundred-plus entities, from publicly traded companies to energy coöperatives, that together constitute the power grid. As a result, transformers can’t be stockpiled. They are almost always built to spec, and they are almost all made abroad, which increases shipping times and leaves them vulnerable to political conflict and supply-chain issues. Even under optimal circumstances, the typical lead time to replace a transformer is at least a year.
