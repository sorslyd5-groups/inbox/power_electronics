---
layout: default
title: Thermals
nav_order: 7
mathjax: true
---

# Thermals
{: .no_toc}

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

- most heating in power electronics is due to Joule heating (also known as resisitve or Ohmic heating)
- due to heating caused by current passing through a conductor
- P = i^2 * r law
    - at high currents, resistance starts to matter quite a bit

## thermal design considerations
- active vs passive cooling
    - active refers to using a fluid cooling setup (air or liquid) that actively removes heat from a system
    - passive refers to relying purely on convection created from thermal gradients created within the system
- thermal design is system design
    - the entire thermal path must be considered when designing for thermals
    - even if you have a beefy connector, a thin cable will still turn into a fuse
- connector selection
    - current rating
    - V0 flame rating
- cable gauge
    - for a connector, the cable is typically treated as a heatsink
- pcb copper weight
    - 1oz is "normal"
    - 4oz is considered power pcb
    - anything higher is considered exotic in pcb land (and gets really expensive really quickly)
    - busbars and jumpers, can get the equivalent of 10-20oz copper very cheaply
    - solder-reinforcing traces, another cheap way to get thick traces
- heatsinking and thermal interface materials (TIMs)
    - convection: hot air rises, so orient fins in direction of gravity
- heatpipes
    - sealed components containing a gas/liquid that undergoes phase transition based on temperature

Thermal conductivities table

| Material                     | Thermal Conductivity (W/m*K)  |
|------------------------------|------------------------------ |
| Diamond                      | 500                           |
| Copper                       | 300                           |
| Aluminum                     | 240                           |
| Steel                        | 40                            |
| Thermally Conductive Plastic | 2.5                           |
| Plastic                      | 0.4                           |

## thermal simulation principles
- thermal simulation
    - IC simulation models
        - two-resistor model (JESD15-3)
        - delphi model (JESD15-4)
    - transient vs steady state modeling
	- ![](media/Pasted image 20240226215807.png)
	- https://fscdn.rohm.com/en/products/databook/applinote/common/two_resistor_model_for_thermal_simulation-e.pdf
	- ![](media/Pasted image 20240226215906.png)
	- https://www.electronics-cooling.com/2019/11/jedec-thermal-standards-developing-a-common-understanding/

## thermal simulation tools

using the above IC simulation models, you can run reasonably accurate thermal simulations using generic fluid simulation software.
however, many CAD tools come with dedicated simulation tools for electronics cooling studies:

- fusion360 - electronics cooling study
    - https://help.autodesk.com/view/fusion360/ENU/?guid=SIM-E-COOLING-SDY-CONCEPT
- solidworks - electronics cooling module
    - https://www.javelin-tech.com/3d/technology/solidworks-electronic-cooling/
- ansys Icepak
    - https://www.ansys.com/products/electronics/ansys-icepak
- FloEFD
    - https://plm.sw.siemens.com/en-US/simcenter/fluids-thermal-simulation/floefd/
- comsol

---

# supplementary materials

- thermal
    - https://fscdn.rohm.com/en/products/databook/applinote/common/two_resistor_model_for_thermal_simulation-e.pdf
- electronics packaging
    - https://ieeexplore.ieee.org/document/9361621
