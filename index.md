---
title: Home
layout: home
nav_order: 1
---

# Power electronics

## Passive

- resistors
- capacitors
- inductors
 
## Diodes and transistors

- Diodes
- FETs
- Reading a datasheet

## transformers, relays

- AC to AC
- AC to DC

## regulators

- voltage divider
- LDO
- Zener reference
- switching regulators

## drivers

- DC motor: H-bridge
- BLDC: tri-phase
- stepper: current sensing

## thermals

- design considerations
- simulation
