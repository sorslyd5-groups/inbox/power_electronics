#!/usr/bin/env python3
import argparse
import os
import shutil

def copy_and_rename_directory(topics_file, source_directory, destination_directory):
    # Read topics from the file
    with open(topics_file, 'r') as file:
        topics = file.read().splitlines()

    # Ensure the destination directory exists
    os.makedirs(destination_directory, exist_ok=True)

    for topic in topics:
        # Generate the new directory path based on the topic and destination directory
        new_directory = os.path.join(destination_directory, topic)
        
        # Copy the directory
        shutil.copytree(source_directory, new_directory)
        print(f"Copied and renamed {source_directory} to {new_directory}")

if __name__ == "__main__":
    # Setup argument parser
    parser = argparse.ArgumentParser(description="Copy a directory for each topic listed in a file, placing them in a specified destination directory and renaming accordingly.")
    parser.add_argument("topics_file", type=str, help="Path to the file containing the list of topics.")
    parser.add_argument("source_directory", type=str, help="Path to the source directory to be copied.")
    parser.add_argument("destination_directory", type=str, help="Path to the destination directory where the copies will be placed.")

    # Parse arguments
    args = parser.parse_args()

    # Execute the main function
    copy_and_rename_directory(args.topics_file, args.source_directory, args.destination_directory)

